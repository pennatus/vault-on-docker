# Vault On Docker

This repository is used to build a Docker image that runs Hashicorp's
Vault: <https://www.vaultproject.io/> inside a Docker container with the intention
of deploying the container on GCP Cloud Run service.

#!/bin/sh
export SKIP_SETCAP=1

/usr/local/bin/tinyproxy-bootstrap.sh &

ARCH=`arch`

if [ "$SQL_INSTANCES" != "" ]; then
  if ["$ARCH" == "aarch64"]; then
    CLOUD_SQL_PROXY=/usr/local/bin/cloud_sql_proxy_arm64
  else
    CLOUD_SQL_PROXY=/usr/local/bin/cloud_sql_proxy_amd64
  fi
  $CLOUD_SQL_PROXY -dir=/cloudsql -instances=$SQL_INSTANCES
fi

exec /usr/local/bin/docker-entrypoint.sh server

#!/bin/sh

# Wait at most 15 seconds for Vault status to succeed (vault is unsealed) before
# enabling the proxy that will forward our 8080 port to the internal Vault port of 7200
i=1;
while [[ $i -le 10 ]] ;
do
  echo $i;
  i=$((i+1));
  if vault status; then
    break;
  fi
  sleep 1
done;

exec tinyproxy
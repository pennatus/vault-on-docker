FROM vault:1.12.2

RUN wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O /usr/local/bin/cloud_sql_proxy.amd64 && chmod +x /usr/local/bin/cloud_sql_proxy.amd64
RUN wget https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.arm64 -O /usr/local/bin/cloud_sql_proxy.arm64 && chmod +x /usr/local/bin/cloud_sql_proxy.arm64

COPY docker-entrypoint.sh /usr/local/bin/.
COPY start.sh /usr/local/bin/.
COPY tinyproxy-bootstrap.sh /usr/local/bin/.

RUN apk add tinyproxy

COPY tinyproxy.conf /etc/tinyproxy/.

EXPOSE 8080
CMD [ "/usr/local/bin/start.sh" ]
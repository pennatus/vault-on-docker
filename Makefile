DOCKER_TAG := 1.0.0
DOCKER_PATH := gcr.io/ioka-docker-images/vault-on-docker

BUILD_CMD:=docker buildx build --platform linux/amd64,linux/arm64

build:
	docker buildx create --use	
	$(BUILD_CMD) .

deploy:
	echo -n $(DOCKER_PASSWORD) | base64 -d | docker login -u $(DOCKER_USER) --password-stdin gcr.io
	docker buildx create --use	
	$(BUILD_CMD) --push -t $(DOCKER_PATH):$(DOCKER_TAG) .
	$(BUILD_CMD) --push -t $(DOCKER_PATH):latest .